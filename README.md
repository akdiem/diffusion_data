# A Simulation Model of Periarterial Clearance of Amyloid-beta from the Brain #


Data produced using COMSOL Multiphysics 4.4.


**Citation:** Diem AK, Tan M, Bressloff NW, Hawkes C, Morris AWJ, Weller RO and Carare RO (2016). A Simulation Model of Periarterial Clearance of Amyloid-beta from the Brain. Front. Aging Neurosci. 8:18. doi: 10.3389/fnagi.2016.00018



## Contents ##

coronal/

	A1/
		#ds.dat 	Positions in coronal slice at time #d in s for simulation case A1. Color (0/1) indicates presence of tracer at the current position
	A2/
	A3/
	B1/
	B2/
	B3/
	C1/
	C2/
	C3/
	C4/	#ds.dat		Analogous to contents of folder A1, but for the respective simulation case

	A1_max_spread.dat	Maximum distance tracer has travelled at various time points. Data collected from data in A1/
	A2_max_spread.dat
	A3_max_spread.dat
	B1_max_spread.dat	
	B2_max_spread.dat
	B3_max_spread.dat
	C1_max_spread.dat
	C2_max_spread.dat
	C3_max_spread.dat
	C4_max_spread.dat	Analogous to A1_max_spread.dat, but for the respective simulation case

	mesh3.csv		Concentration of tracer at 3 points in the geometry for Mesh 3. Used to determine mesh convergence
	mesh6.csv
	mesh10.csv
	mesh12.csv
	mesh13.csv
	mesh14.csv
	mesh15.csv		Analogous to mesh3.csv, but for the respective mesh resolution.


sagittal/

	A1/
		#ds.dat 	Positions in sagittal slice at time #d in s for simulation case A1. Color (0/1) indicates presence of tracer at the current position
	A2/
	A3/
	B1/
	B2/
	B3/
	C1/
	C2/
	C3/
	C4/	#ds.dat		Analogous to contents of folder A1, but for the respective simulation case

	A1_max_spread.dat	Maximum distance tracer has travelled at various time points. Data collected from data in A1/
	A2_max_spread.dat
	A3_max_spread.dat
	B1_max_spread.dat	
	B2_max_spread.dat
	B3_max_spread.dat
	C1_max_spread.dat
	C2_max_spread.dat
	C3_max_spread.dat
	C4_max_spread.dat	Analogous to A1_max_spread.dat, but for the respective simulation case

	mesh1.csv		Concentration of tracer at 3 points in the geometry for Mesh 1. Used to determine mesh convergence
	mesh2.csv
	mesh3.csv
	mesh4.csv
	mesh5.csv
	mesh6.csv
	mesh7.csv		Analogous to mesh3.csv, but for the respective mesh resolution.